package com.example.a20231108_tylerdentremont_nycschools.utils

sealed class ResponsesState {
    object Loading : ResponsesState()
    class Success<T>(val response: T) : ResponsesState()
    class Error(val error: Throwable) : ResponsesState()
}
