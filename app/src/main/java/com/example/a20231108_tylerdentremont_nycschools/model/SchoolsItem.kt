package com.example.a20231108_tylerdentremont_nycschools.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SchoolsItem(
    @Json(name = "dbn")
    val dbn: String,
    @Json(name = "overview_paragraph")
    val overview_paragraph: String? = null,
    @Json(name = "school_email")
    val schoolEmail: String? = null,
    @Json(name = "school_name")
    val schoolName: String? = null,
    @Json(name = "website")
    val website: String? = null,
    @Json(name = "zip")
    val zip: String? = null
)