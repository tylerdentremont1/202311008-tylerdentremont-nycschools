package com.example.a20231108_tylerdentremont_nycschools.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Score(
    @Json(name = "dbn")
    val dbn: String,
    @Json(name = "num_of_sat_test_takers")
    val numOfSatTestTakers: String,
    @Json(name = "sat_critical_reading_avg_score")
    val satCriticalReadingAvgScore: String,
    @Json(name = "sat_math_avg_score")
    val satMathAvgScore: String,
    @Json(name = "sat_writing_avg_score")
    val satWritingAvgScore: String,
    @Json(name = "school_name")
    val schoolName: String
)