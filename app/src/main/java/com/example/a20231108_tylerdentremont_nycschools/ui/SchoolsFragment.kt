package com.example.a20231108_tylerdentremont_nycschools.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20231108_tylerdentremont_nycschools.R
import com.example.a20231108_tylerdentremont_nycschools.adapter.SchoolAdapter
import com.example.a20231108_tylerdentremont_nycschools.databinding.FragmentSchoolsBinding
import com.example.a20231108_tylerdentremont_nycschools.model.SchoolsItem
import com.example.a20231108_tylerdentremont_nycschools.utils.ResponsesState
import com.example.a20231108_tylerdentremont_nycschools.viewmodel.SchoolsViewModel

class SchoolsFragment : Fragment() {

    private val binding by lazy {
        FragmentSchoolsBinding.inflate(layoutInflater)
    }

    private val schoolViewModel by lazy {
        ViewModelProvider(requireActivity())[SchoolsViewModel::class.java]
    }

    private val schoolAdapter by lazy {
        SchoolAdapter {
            // here we will move to the details fragment
            schoolViewModel.currentSchool = it
            findNavController().navigate(R.id.SchoolsFragment_to_DetailsFragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding.schoolRv.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = schoolAdapter
        }

        schoolViewModel.schools.observe(viewLifecycleOwner) { state ->
            when (state) {
                is ResponsesState.Loading -> {
                    Toast.makeText(requireContext(), "Loading...", Toast.LENGTH_SHORT).show()
                }
                is ResponsesState.Success<*> -> {
                    val data = state.response as List<SchoolsItem>
                    schoolAdapter.setNewData(data)

                }
                is ResponsesState.Error -> {
                    Toast.makeText(requireContext(), "Error happened ${state.error.localizedMessage}", Toast.LENGTH_LONG).show()
                    Log.e("SF", "onCreateView: "+ state.error.localizedMessage, state.error)
                }
            }
        }
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        schoolViewModel.getAllSchools()
    }
}