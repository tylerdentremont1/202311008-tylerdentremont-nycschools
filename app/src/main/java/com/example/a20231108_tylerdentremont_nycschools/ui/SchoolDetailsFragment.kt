package com.example.a20231108_tylerdentremont_nycschools.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.a20231108_tylerdentremont_nycschools.R
import com.example.a20231108_tylerdentremont_nycschools.databinding.FragmentSchoolDetailsBinding
import com.example.a20231108_tylerdentremont_nycschools.model.Score
import com.example.a20231108_tylerdentremont_nycschools.utils.ResponsesState
import com.example.a20231108_tylerdentremont_nycschools.viewmodel.SchoolsViewModel

class SchoolDetailsFragment : Fragment() {

    private val binding by lazy{
        FragmentSchoolDetailsBinding.inflate(layoutInflater)
    }

    private val schoolViewModel by lazy{
        ViewModelProvider(requireActivity())[SchoolsViewModel::class.java]
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        schoolViewModel.satScores.observe(viewLifecycleOwner) { state ->
            when (state) {
                is ResponsesState.Loading -> {
                    Toast.makeText(requireContext(), "Loading...", Toast.LENGTH_SHORT).show()
                }
                is ResponsesState.Success<*> -> {
                    val data = state.response as List<Score>
                    setDetails(data)
                }
                is ResponsesState.Error -> {
                    Toast.makeText(requireContext(), "Error happened ${state.error.localizedMessage}", Toast.LENGTH_LONG).show()
                    Log.d("SF", "onCreateView32: "+ state.error.localizedMessage)
                }
            }
        }

        schoolViewModel.currentSchool?.let {
            schoolViewModel.getSATScores(it.dbn)
        }

        return binding.root
    }

    private fun setDetails(data: List<Score>) {
        if (data.isNotEmpty()) {
            val firstItem = data[0]

            binding.schoolName.text = schoolViewModel.currentSchool?.schoolName
            binding.schoolEmailD.text = schoolViewModel.currentSchool?.schoolEmail
            binding.schoolOverview.text = schoolViewModel.currentSchool?.overview_paragraph

            binding.satMath.text = String.format("Math Score: " + firstItem.satMathAvgScore)
            binding.satReading.text = String.format("Reading Score: " + firstItem.satCriticalReadingAvgScore)
            binding.satWriting.text = String.format("Writing Score: " + firstItem.satWritingAvgScore)
        } else {
            Toast.makeText(requireContext(),"School info doesn't exist",Toast.LENGTH_LONG).show()
            findNavController().navigate(R.id.DetailsFragment_to_SchoolsFragment)
        }
    }
}