package com.example.a20231108_tylerdentremont_nycschools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20231108_tylerdentremont_nycschools.databinding.SchoolItemBinding
import com.example.a20231108_tylerdentremont_nycschools.model.SchoolsItem

class SchoolAdapter(
    private val schools: MutableList<SchoolsItem> = mutableListOf(),
    private val clickHandler: (SchoolsItem) -> Unit
) : RecyclerView.Adapter<SchoolViewHolder>() {

    fun setNewData(newSchools: List<SchoolsItem>) {
        schools.addAll(newSchools)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder =
        SchoolViewHolder(
            SchoolItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) =
        holder.bind(schools[position], clickHandler)

    override fun getItemCount(): Int = schools.size
}

class SchoolViewHolder(
    private val binding: SchoolItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(school: SchoolsItem, clickHandler: (SchoolsItem) -> Unit) {
        binding.schoolName.text = school.schoolName ?: "INVALID NAME"
        binding.schoolEmail.text = school.schoolEmail ?: "INVALID EMAIL"
        binding.detailsBtn.setOnClickListener {
            clickHandler(school)
        }

    }
}