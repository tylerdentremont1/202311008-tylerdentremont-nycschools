package com.example.a20231108_tylerdentremont_nycschools.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

object Service {

    /**
     * This [OkHttpClient] will add the timeouts for connection to the server
     * and the interceptors if needed
     */
    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    private val moshi by lazy {
//        val listMyData = Types.newParameterizedType(List::class.java, SchoolsItem::class.java)
//        val jsonAdapter = moshi.adapter<List<SchoolsItem>>(listMyData)
        Moshi.Builder()
            //.add(Types.newParameterizedType(List::class.java, SchoolsItem::class.java))
            .addLast(KotlinJsonAdapterFactory())
            .build()
    }

    /**
     * This is my retrofit interface service that allow us to make the REST calls to the server API
     */
    val networkService by lazy {
        Retrofit.Builder()
            .baseUrl(NetworkService.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttpClient)
            .build()
            .create(NetworkService::class.java)
    }
}