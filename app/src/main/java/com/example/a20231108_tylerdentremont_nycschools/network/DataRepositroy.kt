package com.example.a20231108_tylerdentremont_nycschools.network

import com.example.a20231108_tylerdentremont_nycschools.model.SchoolsItem
import com.example.a20231108_tylerdentremont_nycschools.model.Score
import retrofit2.Response

interface DataRepository {
    /**
     * This method will retrieve the data from server to get all the schools
     *
     * no parameters needed
     *
     * @return a list of [School]
     */
    suspend fun getAllSchools(): Response<List<SchoolsItem>>

    /**
     * This method will retrieve the SAT score for each school
     *
     * @param schoolId - This is the unique ID for each school to search
     *
     * @return a list of [Score] - This list could return only one item
     */
    suspend fun getScoresBySchoolId(schoolId: String): Response<List<Score>>
}

class DataRepositoryImpl(
    private val service: NetworkService = Service.networkService
) : DataRepository {

    override suspend fun getAllSchools(): Response<List<SchoolsItem>> =
        service.getAllSchools()

    override suspend fun getScoresBySchoolId(schoolId: String): Response<List<Score>> =
        service.getScoresBySchoolId(schoolId)
}