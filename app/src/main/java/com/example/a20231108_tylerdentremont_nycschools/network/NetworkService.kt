package com.example.a20231108_tylerdentremont_nycschools.network

import com.example.a20231108_tylerdentremont_nycschools.model.SchoolsItem
import com.example.a20231108_tylerdentremont_nycschools.model.Score
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkService {

    @GET(ALL_SCHOOLS)
    suspend fun getAllSchools(): Response<List<SchoolsItem>>

    @GET(SCORES)
    suspend fun getScoresBySchoolId(
        @Query("dbn") schoolId: String
    ): Response<List<Score>>


    companion object {
        // get schools : https://data.cityofnewyork.us/resource/s3k6-pzi2.json
        // get SAT scores: https://data.cityofnewyork.us/resource/f9bf-2cp4.json
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"
        private const val ALL_SCHOOLS = "s3k6-pzi2.json"
        private const val SCORES = "f9bf-2cp4.json"
    }
}