package com.example.a20231108_tylerdentremont_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20231108_tylerdentremont_nycschools.model.SchoolsItem
import com.example.a20231108_tylerdentremont_nycschools.network.DataRepository
import com.example.a20231108_tylerdentremont_nycschools.network.DataRepositoryImpl
import com.example.a20231108_tylerdentremont_nycschools.utils.ResponsesState
import kotlinx.coroutines.*
import kotlin.Exception

class SchoolsViewModel(
    private val schoolRepository: DataRepository = DataRepositoryImpl(),
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val coroutineScope: CoroutineScope = CoroutineScope(SupervisorJob() + ioDispatcher)
) : ViewModel(), CoroutineScope by coroutineScope {

    private val _schools: MutableLiveData<ResponsesState> = MutableLiveData()
    val schools: LiveData<ResponsesState> get() = _schools

    private val _satScores: MutableLiveData<ResponsesState> = MutableLiveData()
    val satScores: LiveData<ResponsesState> get() = _satScores

    var currentSchool: SchoolsItem? = null

    fun getAllSchools() {
        launch {
            try {
                val mResponse = schoolRepository.getAllSchools()
                if (mResponse.isSuccessful) {
                    mResponse.body()?.let {
                        _schools.postValue(ResponsesState.Success(it))
                    } ?: throw Exception("Response from schools is coming as null")
                } else {
                    throw Exception("Response from schools is a failure")
                }

            } catch (e: Exception) {
                _schools.postValue(ResponsesState.Error(e))
            }
        }
    }

    fun getSATScores(schoolId: String) {
        launch {
            try {
                val scoreResponse = schoolRepository.getScoresBySchoolId(schoolId)
                if (scoreResponse.isSuccessful) {
                    scoreResponse.body()?.let {
                        _satScores.postValue(ResponsesState.Success(it))
                    } ?: throw Exception("Response from SAT scores is coming as null")
                } else {
                    throw Exception("Response from SAT scores is a failure")
                }

            } catch (e: Exception) {
                _satScores.postValue(ResponsesState.Error(e))
            }
        }
    }


}